#!/usr/bin/python
import sys
import argparse
import json
import time
from kubernetes import client
from kubernetes.client.rest import ApiException
from primage_k8s import k8sclient

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='test pods')
    parser.add_argument('--host', type=str, default='https://upv.datahub.egi.eu:6443',
                    help='endpoint of the K8s front end (Default: https://upv.datahub.egi.eu:6443)')
    parser.add_argument('--token', type=str,
                    default='848spfalg64v176hbhgkjbasdfs1sd2afdkj162a5dg4f1jb4hk262464xs6dgh14tjh',
                    help='Token for the K8s front end (Default: my token')
    parser.add_argument('--namespace', type=str,
                    default='iblanque-at-dsic-dot-upv-dot-es',
                    help='namespace (Default: my namespace')
    parser.add_argument('--name', type=str, default='primage-pod',
                    help='name of the pod (Default: primage-pod')
    parser.add_argument('--container', type=str, default='busybox', help='Container image (Default: busybox)')
    parser.add_argument('--command', type=str, default='sleep 3600', help='Command (args) (Default: sleep 3600)')
    args = parser.parse_args()

  
    host = args.host
    token = args.token
    command = args.command
    podname = args.name
    container = args.container
    command = [args.command] 
    namespace = args.namespace
    client = k8sclient(host, token)
    ret = client.list_pods(args.namespace)
    for i in ret.items:
        print("%s  %s  %s" % (i.status.pod_ip, i.metadata.namespace, i.metadata.name))

    print(">>>>>>>> Before creating the POD.")
    time.sleep(4)

    client.create_pod(podname, container, command, namespace)
    time.sleep(4)
    ret = client.list_pods(args.namespace)
    for i in ret.items:
        print("%s  %s  %s" % (i.status.pod_ip, i.metadata.namespace, i.metadata.name))

    print(">>>>>>>> After creating the POD.")

    client.delete_pod(podname, namespace)
    time.sleep(4)
    ret = client.list_pods(args.namespace)
    for i in ret.items:
        print("%s  %s  %s" % (i.status.pod_ip, i.metadata.namespace, i.metadata.name))

    print(">>>>>>>> After deleting the POD.")

