#!/usr/bin/python
import sys
import argparse
import json
import time
from kubernetes import client
from kubernetes.client.rest import ApiException
from primage_k8s import k8sclient

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='test jobs')
    parser.add_argument('--host', type=str, default='https://upv.datahub.egi.eu:6443',
                    help='endpoint of the K8s front end (Default: https://upv.datahub.egi.eu:6443)')
    parser.add_argument('--token', type=str,
                    default='848spfalg64v176hbhgkjbasdfs1sd2afdkj162a5dg4f1jb4hk262464xs6dgh14tjh',
                    help='Token for the K8s front end (Default: my token')
    parser.add_argument('--namespace', type=str,
                    default='iblanque-at-dsic-dot-upv-dot-es',
                    help='namespace (Default: my namespace')
    parser.add_argument('--name', type=str, default='primage-job',
                    help='name of the job (Default: primage-job')
    parser.add_argument('--container', type=str, default='perl', help='Container image (Default: busybox)')
#    parser.add_argument('--command', type=str, default='sleep 60', help='Command (args) (Default: sleep 3600)')
    args = parser.parse_args()

  
    host = args.host
    token = args.token
    command = ["perl", "-Mbignum=bpi", "-wle", "print bpi(2000)"]
    jobname = args.name
    container = args.container
    namespace = args.namespace
    client = k8sclient(host, token)
    ret = client.list_jobs(namespace)
    for i in ret.items:
        print("%s  %s" % (i.metadata.namespace, i.metadata.name))

    print(">>>>>>>> Before creating the Job.")
    time.sleep(4)

    client.create_job(jobname, container, command, namespace)
    time.sleep(4)
    ret = client.list_jobs(args.namespace)
    for i in ret.items:
        print("%s  %s" % (i.metadata.namespace, i.metadata.name))

    print(">>>>>>>> After creating the Job.")

    client.delete_job(jobname, namespace)
    time.sleep(4)
    ret = client.list_jobs(args.namespace)
    for i in ret.items:
        print("%s  %s" % (i.metadata.namespace, i.metadata.name))

    print(">>>>>>>> After deleting the Job.")

