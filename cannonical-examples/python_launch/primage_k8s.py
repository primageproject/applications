#!/usr/bin/python
import sys
import argparse
import json
from kubernetes import client
from kubernetes.client.rest import ApiException
from pprint import pprint

class k8sclient:
    def __init__(self, host, token):
        # This is necessary for accessing a remote endpoint
        self.configuration = client.Configuration()
        self.configuration.host = host
        self.configuration.verify_ssl = False
        self.configuration.debug = False
        self.configuration.api_key = {"authorization": "Bearer " + token}
        self.api_client = client.ApiClient(self.configuration)
        self.CoreV1=client.CoreV1Api()
        self.BatchV1=client.BatchV1Api(client.ApiClient(self.configuration))

    def list_pods(self, namespace):
        # Now we have a client
        api=self.CoreV1

        # List PODs
        try:
            ret = api.list_namespaced_pod(namespace=namespace)
        except ApiException as e:
            print("Exception when calling CoreV1Api->list_namespaced_pod: %s\n" % e)
        return ret

    def create_pod(self, name, container_image, command, namespace):
        # Create a POD
        pod=client.V1Pod()
        spec=client.V1PodSpec()
        pod.metadata=client.V1ObjectMeta(name=name)
        container=client.V1Container()
        container.image=container_image
        container.args=command
        container.name=name
        spec.containers = [container]
        pod.spec = spec
        api = self.CoreV1
        try:
            rc = api.create_namespaced_pod(namespace=namespace, body=pod)
            return rc
        except ApiException as e:
            print("Exception when calling CoreV1Api->create_namespaced_pod: %s\n" % e)
   
    def delete_pod(self, name, namespace):
        pretty = 'pretty_example' # str | If 'true', then the output is pretty printed. (optional)
        body = client.V1DeleteOptions() # V1DeleteOptions |  (optional)
        api = self.CoreV1
        try:
            rc = api.delete_namespaced_pod(name, namespace, pretty=pretty, body=body)
            return rc
        except ApiException as e:
            print("Exception when calling CoreV1Api->delete_namespaced_pod: %s\n" % e)

    def list_jobs(self, namespace):
        # Now we have a client
        api=self.BatchV1

        # List Jobs
        try:
            ret = api.list_namespaced_job(namespace=namespace)

        except ApiException as e:
            print("Exception when calling CoreV1Api->list_namespaced_pod: %s\n" % e)
        return ret

    def create_job(self, name, container_image, command, namespace):
        # Create a POD
        job=client.V1Job()
        spec=client.V1PodSpec()
        job.metadata=client.V1ObjectMeta(name=name)
        container=client.V1Container()
        container.image=container_image
        container.args=command
        container.name=name

        template = client.V1PodTemplateSpec(metadata=client.V1ObjectMeta(labels={"app": name}),spec=client.V1PodSpec(restart_policy="Never", containers=[container]))
        spec = client.V1JobSpec(template=template)
        api=self.BatchV1
        job = client.V1Job(api_version="batch/v1", kind="Job", metadata=client.V1ObjectMeta(name=name), spec=spec)
        try:
            rc = api.create_namespaced_job(namespace=namespace, body=job)
            return rc
        except ApiException as e:
            print("Exception when calling CoreV1Api->create_namespaced_job: %s\n" % e)
   
    def delete_job(self, name, namespace):
        pretty = 'pretty_example' # str | If 'true', then the output is pretty printed. (optional)
        body = client.V1DeleteOptions() # V1DeleteOptions |  (optional)
        api=self.BatchV1
        try:
            rc = api.delete_namespaced_job(name, namespace, pretty=pretty, body=body)
            return rc
        except ApiException as e:
            print("Exception when calling CoreV1Api->delete_namespaced_job: %s\n" % e)

