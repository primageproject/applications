#!/usr/bin/python
import sys
import argparse
import json
from kubernetes import client
from kubernetes.client.rest import ApiException
from primage_k8s import k8sclient

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='list pods')
    parser.add_argument('--host', type=str, default='https://upv.datahub.egi.eu:6443',
                    help='endpoint of the K8s front end (Default: https://upv.datahub.egi.eu:6443)')
    parser.add_argument('--token', type=str,
                    default='848spfalg64v176hbhgkjbasdfs1sd2afdkj162a5dg4f1jb4hk262464xs6dgh14tjh',
                    help='Token for the K8s front end (Default: my token')
    parser.add_argument('--namespace', type=str,
                    default='iblanque-at-dsic-dot-upv-dot-es',
                    help='namespace (Default: my namespace')
    args = parser.parse_args()

    host = args.host
    token = args.token
    client = k8sclient(host, token)
    ret = client.list_pods(args.namespace)
    for i in ret.items:
        print("%s  %s  %s" % (i.status.pod_ip, i.metadata.namespace, i.metadata.name))
