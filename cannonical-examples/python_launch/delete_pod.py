#!/usr/bin/python
import sys
import argparse
import json
from kubernetes import client
from kubernetes.client.rest import ApiException

def set_configuration(host, token):
    # This is necessary for accessing a remote endpoint
    configuration = client.Configuration()
    configuration.host = host
    configuration.verify_ssl = False
    configuration.debug = False
    configuration.api_key = {"authorization": "Bearer " + token}

    return configuration

def list_pods(configuration, namespace):
    # Now we have a client
    api_client = client.ApiClient(configuration)
    v1=client.CoreV1Api()

    # List PODs
    ret = v1.list_namespaced_pod(namespace=namespace)
    
    return ret

def create_pod(name, container_image, command, namespace):
    # Now we have a client
    v1=client.CoreV1Api()

    # Create a POD
    pod=client.V1Pod()
    spec=client.V1PodSpec()
    pod.metadata=client.V1ObjectMeta(name=name)
    container=client.V1Container()
    container.image=container_image
    container.args=command
    container.name=name
    spec.containers = [container]
    pod.spec = spec
    
    v1.create_namespaced_pod(namespace=namespace, body=pod)
    
if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='list pods')
    parser.add_argument('--host', type=str, default='https://upv.datahub.egi.eu:6443',
                    help='endpoint of the K8s front end (Default: https://upv.datahub.egi.eu:6443)')
    parser.add_argument('--token', type=str,
                    default='848spfalg64v176hbhgkjbasdfs1sd2afdkj162a5dg4f1jb4hk262464xs6dgh14tjh',
                    help='Token for the K8s front end (Default: my token')
    parser.add_argument('--namespace', type=str,
                    default='iblanque-at-dsic-dot-upv-dot-es',
                    help='namespace (Default: my namespace')
    parser.add_argument('--name', type=str, default='primage-pod',
                    help='name of the pod (Default: primage-pod')
    parser.add_argument('--container', type=str, default='busybox', help='Container image (Default: busybox)')
    parser.add_argument('--command', type=str, default='sleep 3600', help='Command (args) (Default: sleep 3600)')
    args = parser.parse_args()
    configuration = set_configuration(args.host, args.token)

    create_pod(args.name, args.container, [args.command], args.namespace)
  
    ret = list_pods(configuration, args.namespace)
    for i in ret.items:
        print("%s  %s  %s" % (i.status.pod_ip, i.metadata.namespace, i.metadata.name))

