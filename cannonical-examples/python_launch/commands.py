#!/usr/bin/python

import json
from kubernetes import client
from kubernetes.client.rest import ApiException

# This is necessary for accessing a remote endpoint
configuration = client.Configuration()
configuration.host = 'https://upv.datahub.egi.eu:6443'
configuration.verify_ssl = False
configuration.debug = False
token = "848spfalg64v176hbhgkjbasdfs1sd2afdkj162a5dg4f1jb4hk262464xs6dgh14tjh"
configuration.api_key = {"authorization": "Bearer " + token}
api_client = client.ApiClient(configuration)

# Now we have a client
v1=client.CoreV1Api()

# List PODs
ret = v1.list_namespaced_pod(namespace="iblanque-at-dsic-dot-upv-dot-es")
/usr/lib/python2.7/dist-packages/urllib3/connectionpool.py:860: InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
  InsecureRequestWarning)
for i in ret.items:
    print("%s  %s  %s" % (i.status.pod_ip, i.metadata.namespace, i.metadata.name))

10.253.1.208  iblanque-at-dsic-dot-upv-dot-es  creating-primage-configmap-vvzkf
10.253.1.132  iblanque-at-dsic-dot-upv-dot-es  frameext-htc-batch-queue-service-master
10.253.1.137  iblanque-at-dsic-dot-upv-dot-es  htc-consumer-frameext-ndtcz
10.253.1.136  iblanque-at-dsic-dot-upv-dot-es  htc-consumer-frameext-zt8dk
10.253.1.134  iblanque-at-dsic-dot-upv-dot-es  htc-producer-frameext-nrd2n
10.253.1.86  iblanque-at-dsic-dot-upv-dot-es  jupyter


# Create a POD
pod=client.V1Pod()
spec=client.V1PodSpec()
pod.metadata=client.V1ObjectMeta(name="busybox")
container=client.V1Container()
container.image="busybox"
container.args=["sleep", "3600"]
container.name="busybox"
spec.containers = [container]
pod.spec = spec

v1.create_namespaced_pod(namespace="iblanque-at-dsic-dot-upv-dot-es",body=pod)
/usr/lib/python2.7/dist-packages/urllib3/connectionpool.py:860: InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
  InsecureRequestWarning)
{'api_version': 'v1',
 'kind': 'Pod',
 'metadata': {'annotations': None,
              'cluster_name': None,
              'creation_timestamp': datetime.datetime(2020, 4, 28, 16, 37, 57, tzinfo=tzutc()),
              'deletion_grace_period_seconds': None,
              'deletion_timestamp': None,
              'finalizers': None,
              'generate_name': None,
              'generation': None,
              'labels': None,
              'name': 'busybox',
              'namespace': 'iblanque-at-dsic-dot-upv-dot-es',
              'owner_references': None,
              'resource_version': '10723443',
              'self_link': '/api/v1/namespaces/iblanque-at-dsic-dot-upv-dot-es/pods/busybox',
              'uid': '3258c2f9-7650-4639-b648-14b7fb933f3b'},
 'spec': {'active_deadline_seconds': None,
          'affinity': None,
          'automount_service_account_token': None,
          'containers': [{'args': ['sleep', '3600'],
                          'command': None,
                          'env': None,
                          'env_from': None,
                          'image': 'busybox',
                          'image_pull_policy': 'Always',
                          'lifecycle': None,
                          'liveness_probe': None,
                          'name': 'busybox',
                          'ports': None,
                          'readiness_probe': None,
                          'resources': {'limits': None, 'requests': None},
                          'security_context': None,
                          'stdin': None,
                          'stdin_once': None,
                          'termination_message_path': '/dev/termination-log',
                          'termination_message_policy': 'File',
                          'tty': None,
                          'volume_mounts': [{'mount_path': '/var/run/secrets/kubernetes.io/serviceaccount',
                                             'name': 'default-token-q59ct',
                                             'read_only': True,
                                             'sub_path': None}],
                          'working_dir': None}],
          'dns_policy': 'ClusterFirst',
          'host_ipc': None,
          'host_network': None,
          'host_pid': None,
          'hostname': None,
          'image_pull_secrets': None,
          'init_containers': None,
          'node_name': None,
          'node_selector': None,
          'restart_policy': 'Always',
          'scheduler_name': 'default-scheduler',
          'security_context': {'fs_group': None,
                               'run_as_non_root': None,
                               'run_as_user': None,
                               'se_linux_options': None,
                               'supplemental_groups': None},
          'service_account': 'default',
          'service_account_name': 'default',
          'subdomain': None,
          'termination_grace_period_seconds': 30,
          'tolerations': [{'effect': 'NoExecute',
                           'key': 'node.kubernetes.io/not-ready',
                           'operator': 'Exists',
                           'toleration_seconds': 300,
                           'value': None},
                          {'effect': 'NoExecute',
                           'key': 'node.kubernetes.io/unreachable',
                           'operator': 'Exists',
                           'toleration_seconds': 300,
                           'value': None}],
          'volumes': [{'aws_elastic_block_store': None,
                       'azure_disk': None,
                       'azure_file': None,
                       'cephfs': None,
                       'cinder': None,
                       'config_map': None,
                       'downward_api': None,
                       'empty_dir': None,
                       'fc': None,
                       'flex_volume': None,
                       'flocker': None,
                       'gce_persistent_disk': None,
                       'git_repo': None,
                       'glusterfs': None,
                       'host_path': None,
                       'iscsi': None,
                       'name': 'default-token-q59ct',
                       'nfs': None,
                       'persistent_volume_claim': None,
                       'photon_persistent_disk': None,
                       'portworx_volume': None,
                       'projected': None,
                       'quobyte': None,
                       'rbd': None,
                       'scale_io': None,
                       'secret': {'default_mode': 420,
                                  'items': None,
                                  'optional': None,
                                  'secret_name': 'default-token-q59ct'},
                       'vsphere_volume': None}]},
 'status': {'conditions': None,
            'container_statuses': None,
            'host_ip': None,
            'init_container_statuses': None,
            'message': None,
            'phase': 'Pending',
            'pod_ip': None,
            'qos_class': 'BestEffort',
            'reason': None,
            'start_time': None}}
ret = v1.list_namespaced_pod(namespace="iblanque-at-dsic-dot-upv-dot-es")
/usr/lib/python2.7/dist-packages/urllib3/connectionpool.py:860: InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
  InsecureRequestWarning)

# List PODs
for i in ret.items:
    print("%s  %s  %s" % (i.status.pod_ip, i.metadata.namespace, i.metadata.name))

10.253.1.152  iblanque-at-dsic-dot-upv-dot-es  busybox
10.253.1.208  iblanque-at-dsic-dot-upv-dot-es  creating-primage-configmap-vvzkf
10.253.1.132  iblanque-at-dsic-dot-upv-dot-es  frameext-htc-batch-queue-service-master
10.253.1.137  iblanque-at-dsic-dot-upv-dot-es  htc-consumer-frameext-ndtcz
10.253.1.136  iblanque-at-dsic-dot-upv-dot-es  htc-consumer-frameext-zt8dk
10.253.1.134  iblanque-at-dsic-dot-upv-dot-es  htc-producer-frameext-nrd2n
10.253.1.86  iblanque-at-dsic-dot-upv-dot-es  jupyter
