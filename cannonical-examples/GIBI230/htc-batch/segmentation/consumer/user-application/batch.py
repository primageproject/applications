import os
import logging
import radiomics
from radiomics import featureextractor
import SimpleITK as sitk
import pandas as pd

#INPUT
#    ImageName --> Ruta de la iimegen de entrada
#    maskName  --> Ruta de la mascara de entrada
#    outputFile --> Ruta del fichero de salida

def start_batch(imageName, maskName, outputFile): 

    # ******************************************************************************************************************************  
    # ******************************************************************************************************************************  
    # ******************************************************************************************************************************  
    # ******************************************************************************************************************************  
    f=os.path.basename(imageName)
    ff=f.split("_")[0]
    nameT2 = "T2"

    print("**********************************************************************************************************************")
    print("**********************************************************************************************************************")
    print("Working on imageName: " + imageName)
    print("                  ff: " + ff)
    print("            maskName: " + maskName)
    print("          outputfile: " + outputFile)

    if imageName is None or maskName is None:  # Something went wrong, in this case PyRadiomics will also log an error
        print("Error getting testcase!")
        return
    if os.path.isfile(maskName) is False:
        print("No mask for this file")
        return

    # Get the PyRadiomics logger (default log-level = INFO)
    logger = radiomics.logger
    logger.setLevel(logging.DEBUG)  # set level to DEBUG to include debug log messages in log file

    handler = logging.FileHandler(filename='testLog.txt', mode='w')
    formatter = logging.Formatter("%(levelname)s:%(name)s: %(message)s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    # Define settings for signature calculation
    # These are currently set equal to the respective default values
    settings = {}
    #settings['binWidth'] = 0.000047 #ADC
    settings['binWidth'] = 5 #T2
    settings['resampledPixelSpacing'] = None  # [3,3,3] is an example for defining resampling (voxels with size 3x3x3mm)
    settings['interpolator'] = sitk.sitkBSpline
    settings['normalize'] = True 
    settings['normalizeScale'] = 100
    settings['voxelArrayShift'] = 300
    
    settings['resegmentRange'] = [-3, 3]
    settings['resegmentMode'] = 'sigma'
    
    # Initialize feature extractor
    extractor = featureextractor.RadiomicsFeatureExtractor(**settings)
    
    # Disable all classes except firstorder
    #extractor.disableAllFeatures()
    
    # By default, only original is enabled. Optionally enable some image types:
    #extractor.enableImageTypes(Original={}, LoG={}, Wavelet={}, Square={}, SquareRoot={}, Logarithm={}, Exponential={}, Gradient={})
    extractor.enableImageTypes(Original={}, LoG={'sigma' : [1.0, 3.0, 5.0, 7.0]}, Square={}, SquareRoot={}, Logarithm={}, Exponential={})
    # , LoG={'sigma' : [1.0, 3.0, 5.0, 7.0]}, Square={}, SquareRoot={}, Logarithm={}, Exponential={}
    
    # Disable all classes except firstorder
    #extractor.disableAllFeatures()
    
    # Enable all features in firstorder
    #extractor.enableFeatureClassByName('firstorder')
    
    # Only enable mean and skewness in firstorder
    #extractor.enableFeaturesByName(firstorder=['Mean', 'Skewness'])
    
    #print("Calculating features")
    featureVector = extractor.execute(imageName, maskName)
    
    extractor_f1 = featureextractor.RadiomicsFeatureExtractor(**settings)
    extractor_f1.enableImageTypes(Wavelet={'wavelet' : 'coif1'})
    #print("Calculating features filter 1")
    featureVector_f1 = extractor_f1.execute(imageName, maskName)
    
    extractor_f2 = featureextractor.RadiomicsFeatureExtractor(**settings)
    extractor_f2.enableImageTypes(Wavelet={'wavelet' : 'haar'})
    #print("Calculating features filter 2")
    featureVector_f2 = extractor_f2.execute(imageName, maskName)
    
    extractor_f3 = featureextractor.RadiomicsFeatureExtractor(**settings)
    extractor_f3.enableImageTypes(Wavelet={'wavelet' : 'dmey'})
    #print("Calculating features filter 3")
    featureVector_f3 = extractor_f3.execute(imageName, maskName)
    
    extractor_f4 = featureextractor.RadiomicsFeatureExtractor(**settings)
    extractor_f4.enableImageTypes(Wavelet={'wavelet' : 'sym2'})
    #print("Calculating features filter 4")
    featureVector_f4 = extractor_f4.execute(imageName, maskName)


    if not os.path.exists(outputFile+nameT2):
        os.makedirs(outputFile+nameT2)
    
    namefile = "/Textures.csv"
    names_f1 = []
    keys_f1 = []

    for featureName in featureVector_f1.keys():
        #print("Computed %s: %s" % (featureName, featureVector_f1[featureName]))
        if "diagnostics" not in featureName:
            names_f1.append(featureVector_f1[featureName])
            keys_f1.append('coif1_'+featureName)

    names_f2 = []
    keys_f2 = []

    for featureName in featureVector_f2.keys():
        #print("Computed %s: %s" % (featureName, featureVector_f2[featureName]))
        if "diagnostics" not in featureName:
            names_f2.append(featureVector_f2[featureName])
            keys_f2.append('haar_'+featureName)

    names_f3 = []
    keys_f3 = []

    for featureName in featureVector_f3.keys():
        #print("Computed %s: %s" % (featureName, featureVector_f3[featureName]))
        if "diagnostics" not in featureName:
            names_f3.append(featureVector_f3[featureName])
            keys_f3.append('dmey_'+featureName)

    names_f4 = []
    keys_f4 = []

    for featureName in featureVector_f4.keys():
        #print("Computed %s: %s" % (featureName, featureVector_f4[featureName]))
        if "diagnostics" not in featureName:
            names_f4.append(featureVector_f4[featureName])
            keys_f4.append('sym2_'+featureName)

    names = []
    keys = []

    for featureName in featureVector.keys():
        #if "diagnostics" in featureName:
         #   print("Computed %s: %s" % (featureName, featureVector[featureName]))
        if "diagnostics" not in featureName:
            names.append(featureVector[featureName])
            keys.append(featureName)

    df = pd.DataFrame(keys)
    df = df.rename(columns={0: 'type'})
    df_f1 = pd.DataFrame(keys_f1)
    df_f1 = df_f1.rename(columns={0: 'type'})
    df_f2 = pd.DataFrame(keys_f2)
    df_f2 = df_f2.rename(columns={0: 'type'})
    df_f3 = pd.DataFrame(keys_f3)
    df_f3 = df_f3.rename(columns={0: 'type'})
    df_f4 = pd.DataFrame(keys_f4)
    df_f4 = df_f4.rename(columns={0: 'type'})
    result = pd.concat([df, df_f1, df_f2, df_f3, df_f4], ignore_index=True, sort=False)
    #result = pd.concat([df, df_f1], ignore_index=True, sort=False)

    df2 = pd.DataFrame(names)
    df2_f1 = pd.DataFrame(names_f1)
    df2_f2 = pd.DataFrame(names_f2)
    df2_f3 = pd.DataFrame(names_f3)
    df2_f4 = pd.DataFrame(names_f4)
    result2 = pd.concat([df2, df2_f1, df2_f2, df2_f3, df2_f4], ignore_index=True, sort=False)
    #result2 = pd.concat([df2, df2_f1], ignore_index=True, sort=False)
    df_final = result.join(result2)

    df_final_transpose = df_final.set_index("type").T
    export_csv = df_final_transpose.to_csv (outputFile+nameT2+namefile, index = None, header=True)


    #corr = data.corr()
    #ax = sns.heatmap(
    #    corr,
    #    vmin=-1, vmax=1, center=0,
    #    cmap=sns.diverging_palette(20, 220, n=200),
    #    square=True
    #)
    #ax.set_xticklabels(
    #    ax.get_xticklabels(),
    #    rotation=45,
    #    horizontalalignment='right'
    #);
