#!/usr/bin/env python
import os
import sys
import redis_primage_htc_queue
from PRIMAGE_Batch_Args import PRIMAGE_Batch_Args #this is the class that you have to employ to generate the PRIMAGE Batch Arguments Objects

#These two input arguments are mandatory for all producers
primage_htc_batch_queue_service = sys.argv[1]  #First argument corresponds to the Queue service ($PRIMAGE_HTC_BATCH_QUEUE_SERVICE)
queue_name                      = sys.argv[2]  #Second argument corresponds to the name of the queue ($QUEUE_NAME)
#Specific input argumentsfor this producer
path_input_folder_images        = sys.argv[3] #It corresponds to $INPUT_IMAGES
path_input_folder_masks         = sys.argv[4] #It corresponds to $INPUT_MASKS
path_output_folder              = sys.argv[5] #It corresponds to $OUTPUT

#First, the producer creates a Queue named $QUEUE_NAME from PRIMAGE HTC Queue Service (value from $PRIMAGE_HTC_BATCH_QUEUE_SERVICE).
primage_htc_queue = redis_primage_htc_queue.RedisWQ(name=queue_name, host=primage_htc_batch_queue_service)
print("Push with sessionID: " +  primage_htc_queue.sessionID())
print("Initial queue state: empty=" + str(primage_htc_queue.empty()))

for root2, dirs, files in os.walk(path_input_folder_images, topdown=False):
    if root2.endswith("T2") and "Otros" not in root2 and "OTROS" not in root2:
        for file in os.listdir(root2):
            if file.endswith('.nii'):
                file_path    = os.path.join(root2, file)
                arg          = PRIMAGE_Batch_Args()              
                arg.arg1     = file_path;                        #Arg1. Image path to pprocess
                name_of_file = os.path.basename(file_path)              
                folder       = name_of_file.split("_")[0]
                mask_path    = path_input_folder_masks  + "/" + str(folder) + "/Segmentation_final.nii" #Arg2. Mask path for processing the image
                arg.arg2     = mask_path 
                output_path  = path_output_folder + "/" + str(folder) + "/" #arg3. Output path for results file 
                arg.arg3     = output_path
                print('- Pushing PRIMAGE Batch Argument Object:' + str(arg))
                primage_htc_queue.push(str(arg))
print("Queue completed")