#!/usr/bin/env python
import sys
import train   #Python module where is implemented the Batch Task

#Get two arguments from the user Comman Line Interface
input_folder  = sys.argv[1]   #the first argument a the input folder path
output_folder = sys.argv[2]   #the second argument is the output folder path

#This is the call to the python module where the Batch task is implemented using the read arguments   
train.start_batch(input_folder, output_folder)