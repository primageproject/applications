#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 15 18:02:05 2019

@author: ajp - modified by xgk
"""

from __future__ import print_function

import cv2
import numpy as np
from keras.callbacks import ModelCheckpoint, Callback
from keras import backend as K
K.set_image_data_format('channels_last')  # TF dimension ordering in this code
from sklearn.model_selection import train_test_split
import models
import newmodels
import os
os.environ['KMP_DUPLICATE_LIB_OK']='True'
from scipy.ndimage import zoom
import matplotlib.pyplot as plt
from skimage.measure import regionprops
from scipy.ndimage import label
from skimage.transform import resize
from newmodels import attn_reg
from keras.optimizers import Adam, SGD, RMSprop
import losses 
import sys

#input_folder  = os.getenv("PRIMAGE_STORAGE") + "/" + os.getenv("INPUT")
#output_folder = os.getenv("PRIMAGE_STORAGE") + "/" + os.getenv("OUTPUT") 
input_folder  = sys.argv[1]
output_folder = sys.argv[2]



def featurewise_normalization(images):
    mean = np.mean(images)
    std = np.std(images)
    np.save(paths['model_save']+'/mean.npy',mean)
    np.save(paths['model_save']+'/std.npy',std)
    images = np.subtract(images,mean)
    images = np.divide(images,std)
    return images

def samplewise_intensity_normalization(images):
    for i in range(images.shape[0]):
        img = images[i,:,:]
        maxim = np.max(img)
        minim = np.min(img)
        if maxim == 0 and minim == 0:
            images[i,:,:] = img
        else:
            images[i,:,:] = (img - minim) / (maxim - minim)
    return images

def augmentation_rotation(image,mask,augmentation_params):
    image_rot = np.copy(image)
    mask_rot = np.copy(mask)
    
    if(flip(augmentation_params["rotation_prob"]) == 'augment'):   
        rows,cols, channels = image.shape
        angle = np.random.uniform(augmentation_params["min_angle"], augmentation_params["max_angle"])
        M = cv2.getRotationMatrix2D((cols/2,rows/2),angle,1)
        image_rot = cv2.warpAffine(image,M,(cols,rows))[..., np.newaxis]
        mask_rot = cv2.warpAffine(mask,M,(cols,rows))[..., np.newaxis]

    return((image_rot,mask_rot))

def augmentation_fliplr(image,mask,augmentation_params):
    
    image_flip = np.copy(image)
    mask_flip = np.copy(mask)
    if(flip(augmentation_params["fliplr_prob"]) == 'augment'):
        image_flip[:, :, 0] = np.fliplr(image[:, :, 0])
        mask_flip[:, :, 0] = np.fliplr(mask[:, :, 0])

    return((image_flip, mask_flip))
    
def transform_matrix_offset_center(matrix, x, y):
    o_x = float(x) / 2 + 0.5
    o_y = float(y) / 2 + 0.5
    offset_matrix = np.array([[1, 0, o_x], [0, 1, o_y], [0, 0, 1]])
    reset_matrix = np.array([[1, 0, -o_x], [0, 1, -o_y], [0, 0, 1]])
    transform_matrix = np.dot(np.dot(offset_matrix, matrix), reset_matrix)
    return transform_matrix

    # function to zoom image based on image.py 
    #https://towardsdatascience.com/image-augmentation-for-deep-learning-using-keras-and-histogram-equalization-9329f6ae5085

#Based at https://stackoverflow.com/questions/37119071/scipy-rotate-and-zoom-an-image-without-changing-its-dimensions/48097478
def clipped_zoom(img, zoom_factor, **kwargs):

    h, w = img.shape[:2]
   # print(h, w)
    # For multichannel images we don't want to apply the zoom factor to the RGB
    # dimension, so instead we create a tuple of zoom factors, one per array
    # dimension, with 1's for any trailing dimensions after the width and height.
    zoom_tuple = (zoom_factor,) * 2 + (1,) * (img.ndim - 2)

    # Zooming out
    if zoom_factor < 1:

        # Bounding box of the zoomed-out image within the output array
        zh = int(np.round(h * zoom_factor))
        zw = int(np.round(w * zoom_factor))
        top = (h - zh) // 2
        left = (w - zw) // 2
        if top<0:
            top = 0
        if left<0:
            left = 0
        # Zero-padding
    #    if top+zh < img.shape[0] and left+zw< 
        out = np.zeros_like(img)
        out[top:top+zh, left:left+zw] = zoom(img, zoom_tuple, **kwargs)

    # Zooming in
    elif zoom_factor > 1:

        # Bounding box of the zoomed-in region within the input array
        zh = int(np.round(h / zoom_factor))
        zw = int(np.round(w / zoom_factor))
        top = (h - zh) // 2
        left = (w - zw) // 2
     #   print(top+zh, left+zw)
        out = zoom(img[top:top+zh, left:left+zw], zoom_tuple, **kwargs)
        if out.shape[0]<img.shape[0] :
              out = zoom(img[top:top+zh+1, left:left+zw+1], zoom_tuple, **kwargs)
        # `out` might still be slightly larger than `img` due to rounding, so
        # trim off any extra pixels at the edges
        trim_top = ((out.shape[0] - h) // 2)
        trim_left = ((out.shape[1] - w) // 2)
    #    print('trim', trim_top, trim_left)
    #    print('size out', out.shape[0], out.shape[1])
# =============================================================================
#         if trim_top+h>
# =============================================================================
        out = out[trim_top:trim_top+h, trim_left:trim_left+w]

    # If zoom_factor == 1, just return the input array
    else:
        out = img
    return out


def augmentation_zoom(image,mask,augmentation_params):
    
    #Create images from the numpy arrays
    image_zoom = np.copy(image)
    mask_zoom = np.copy(mask)
    if(flip(augmentation_params["zoom_prob"]) == 'augment'):
   # if (augmentation_params["zoom_prob"]):
        #Zooming per axis
        if augmentation_params['zoom_axis_same']:
            zx = np.random.uniform(augmentation_params['min_zoom'], augmentation_params['max_zoom'])
            zy = zx
        else:
            zx, zy = np.random.uniform(augmentation_params['min_zoom'], augmentation_params['max_zoom'], 2)

        #Convert the images back to numpy arrays
       # kwargs = {'mode': 'reflect'}
        
        image_zoom = clipped_zoom(image_zoom, zx,  mode = 'reflect')
        
        mask_zoom = clipped_zoom(mask_zoom, zx, mode = 'constant', cval = 0.)
        mask_zoom = (mask_zoom > 0.5).astype(np.int_)
                #Visualize
     #   print(image.shape)
     #   print(zx)
     #   print(image_zoom.shape)
     #   f, ax = plt.subplots(2,2)
     #   ax[0][0].imshow(np.squeeze(image), cmap='gray', vmin=0, vmax=np.amax(image))
     #   ax[0][1].imshow(np.squeeze(image_zoom), cmap='gray', vmin=0, vmax=np.amax(image_zoom))
     #   ax[1][0].imshow(np.squeeze(mask), cmap='gray', vmin=0, vmax=1)
     #   ax[1][1].imshow(np.squeeze(mask_zoom), cmap='gray', vmin=0, vmax=1)
     #   plt.show()
        
    return((image_zoom, mask_zoom))  

def augmentation_random_crop_centroid(image, mask, augmentation_params):
     
    image_crop = np.copy(image)
    mask_crop = np.copy(mask)
    if(flip(augmentation_params["crop_prob"]) == 'augment'):
         #How much to crop from the center of the bounding box - it could be also percentage in future versions
         crop_num = np.round(np.random.uniform(augmentation_params["min_crop"] / 2, augmentation_params["max_crop"]) /2)
         [image_crop,mask_crop] = centroid_crop(image, mask, crop_num)
         f, ax = plt.subplots(2,2)
         ax[0][0].imshow(np.squeeze(image), cmap='gray', vmin=0, vmax=np.amax(image))
         ax[0][1].imshow(np.squeeze(image_crop), cmap='gray', vmin=0, vmax=np.amax(image_crop))
         ax[1][0].imshow(np.squeeze(mask), cmap='gray', vmin=0, vmax=1)
         ax[1][1].imshow(np.squeeze(mask_crop), cmap='gray', vmin=0, vmax=1)
         plt.show()
    return((image_crop, mask_crop))  

 #function to perform crop around the centroid of the object
def centroid_crop(image, mask, crop_perc):
    image_random_crop = np.copy(image);
    mask_random_crop = np.copy(mask);
    #Size of the image
    h, w = image.shape[:2]
    #labeled image or label(mask)
    labeled_image, num_features = label(mask)
    properties = regionprops(labeled_image)
    #Largest object
    obj_areas = [r.area for r in properties]
    largest_obj = np.asscalar(np.array(np.where(obj_areas == np.amax(obj_areas))))
   
    #Center of mass of largest obejct object
    centroid = np.array(properties[largest_obj].centroid);
    #Crop around the largest object by random numbers
    #Top 
    top = int(np.round(centroid[0] - crop_perc))
    if top < 0:
        top = 0
    #
    bottom = int(np.round(centroid[0] + crop_perc))
    if bottom > h:
        bottom = h
    
    left = int(np.round(centroid[1] - crop_perc))
    if left < 0:
        left =  0
        
    right = int(np.round(centroid[1] + crop_perc))
    if right > w:
        right = w
   # print(top, bottom, left, right)
    image_random_crop =  resize(image_random_crop[top:bottom, left:right], (h, w), anti_aliasing=True)
    mask_random_crop = resize(mask_random_crop[top:bottom, left:right], (h, w), anti_aliasing=True)
   
    return((image_random_crop, mask_random_crop)) 

def augmentation_noise(image, augmentation_params):
    
    image_noise = np.copy(image)
    if(flip(augmentation_params["noise_prob"]) == 'augment'):
        mean = 0
        rows, cols, channels = image.shape
        var = np.random.uniform(augmentation_params['min_noise_var'],augmentation_params['max_noise_var'])
        sigma = var**0.5
        gauss = np.random.normal(mean,sigma,(rows,cols, channels))
        image_noise = image + gauss
    
    return image_noise

#Images are randomly augmented or not depending on whether the random number 
#returned is smaller than the provided probability
def flip(p):
    return 'augment' if np.random.random() < p else 'pass'

def training_data_augmentation(img, mask, augmentation_params):
        
#    img_, mask_ = augmentation_fliplr(img, mask, augmentation_params)
   img_, mask_ = augmentation_rotation(img, mask, augmentation_params)
   img_ = augmentation_noise(img_, augmentation_params)
   img_, mask_ = augmentation_zoom(img_, mask_, augmentation_params)
 #  img_, mask_ = augmentation_random_crop_centroid(img_, mask_, augmentation_params)
   img_ = img_.astype('float32')
   mask_ = mask_.astype('float32')
    
   return img_, mask_

def data_generator(X_train, y_train, augmentation_params, batch_size):

    #Generator
   # data_generator_train = ImageDataGenerator(vertical_flip=True)

    # fit parameters from data
  #  datagen.fit(X_train)
    
    batch_images = np.zeros([batch_size, X_train.shape[1], X_train.shape[2], 1])
    batch_masks = np.zeros([batch_size,  y_train.shape[1], y_train.shape[2], 1])
  #  datagen.flow(X_train, y_train, batch_size)
    index = 0
    while True:
        for b in range(batch_size):
            if index == X_train.shape[0]:
                index = 0
                shuffle_indexes = np.arange(X_train.shape[0])
                np.random.shuffle(shuffle_indexes)
                X_train = X_train[shuffle_indexes]
                y_train = y_train[shuffle_indexes]
         #   print(index)
            img, mask = training_data_augmentation(X_train[index], y_train[index], 
                                                 augmentation_params)
         #   pdb.set_trace()
            batch_images[b] = img
            batch_masks[b] = mask
            
            index += 1
        yield batch_images, batch_masks

# =============================================================================
# def image_generator(X_train, y_train, augmentation_params, batch_size):
#       image_gen = ImageDataGenerator(rotation_range=15,
#                                width_shift_range=0.1,
#                                height_shift_range=0.1,
#                                shear_range=0.01,
#                                zoom_range=[0.9, 1.25],
#                                horizontal_flip=True,
#                                vertical_flip=False,
#                                fill_mode='reflect',
#                                data_format='channels_last',
#                                brightness_range=[0.5, 1.5])
# =============================================================================


def select_model(training_params,image_params):
    
    arquitecture = training_params['architecture']
    direction = image_params['direction']
    if direction == 'axial':
        image_rows = image_params['img_rows']
        image_cols = image_params['img_cols']
    elif direction == 'coronal':
        image_rows = image_params['img_slices']
        image_cols = image_params['img_cols']
    elif direction == 'sagital':
        image_rows = image_params['img_slices']
        image_cols = image_params['img_rows']
    else:
        print('Unsoported direction')
        
    if arquitecture == 'unet2D_ds':        
        model = models.get_unet2D_ds(image_rows, image_cols, training_params['learning_rate'])
    
    elif arquitecture == 'get_unet2D_ds_test':
        model = models.get_unet2D_ds(image_rows, image_cols, training_params['learning_rate'], training_params['dilate_rate'])
        
    elif arquitecture == 'attn_reg_ds':
        sgd = SGD(lr=0.01, momentum=0.90, decay=1e-6)
        adam = Adam(lr= training_params['learning_rate'])  
        model = models.get_attention_unet2D(image_rows, image_cols, training_params['learning_rate'], adam);
    else:
        print('Unsupported arquitecture')
    
    return model

def write_params_txt(paths, image_params, augmentation_params, training_params, comments):
    
    experiment = paths['model_save'].split('/')[len(paths['model_save'].split('/')) - 1]
    file = open(paths['model_save'] + '/Params_'+image_params['direction']+'.txt', 'w')
    file.write(experiment + '\n')
    file.write('\n')
    file.write('Preprocess_params \n')
    for key in image_params.keys():
        file.write(key + ': ' + str(image_params[key]) + '\n')
    file.write('\n')
    file.write('Augmentation_params \n')
    for key in augmentation_params.keys():
        file.write(key + ': ' + str(augmentation_params[key]) + '\n')
    file.write('\n')
    file.write('Training_params \n')
    for key in training_params.keys():
        file.write(key + ': ' + str(training_params[key]) + '\n')
    file.write('\n')
    file.write('Comments \n')
    for key in comments.keys():
        file.write(key + ': ' + str(comments[key]) + '\n')
    file.close()

# Class to extract the loss function after each epoch in losses.txt
class LossHistory(Callback):
    
    def __init__(self, model_save_path):
        self.model_save_path = model_save_path
        self.epoch_count = 0
    def on_train_begin(self, logs={}):
        self.losses = []    
        # Erase the content if there is a previous losses file
        self.f = open(self.model_save_path + '/losses.txt', 'w')
        self.f.close()
        
    def on_epoch_end(self, batch, logs={}):
        self.epoch_count = self.epoch_count + 1
        self.losses.append(logs.get('loss'))
        # Open the empty file to append new information
        self.f = open(self.model_save_path + '/losses.txt', 'a')
        self.f.write('Epoch ' + str(self.epoch_count) + ' ' + str(logs) + '\n')
        self.f.close()      
      
def train(paths,image_params,augmentation_params,training_params, comments):
    
    write_params_txt(paths, image_params, augmentation_params, training_params, comments)
        
    print('-'*30)
    print('Loading train data...')
    print('-'*30)
    if training_params['split'] == True: 
        print('Training data is splitted in training and validation (training) set')
        imgs_train = np.load(paths['training_data']+'/imgs_train.npy')
        imgs_mask_train = np.load(paths['training_data']+'/imgs_mask_train.npy')
        
        if image_params['samplewise_intensity_normalization'] == True:
            imgs_train = samplewise_intensity_normalization(imgs_train)
            
        if image_params['featurewise_normalization'] == True:
            imgs_train = featurewise_normalization(imgs_train)
            
       # print('Maxim:', np.max(imgs_train))
       # print('Minim:', np.min(imgs_train))
        X_train, X_val, y_train, y_val = train_test_split(imgs_train, imgs_mask_train, test_size=training_params['val_fraction'], random_state=7)
        training_data = np.savez(paths['model_save']+'/training_data.npz', X_train=X_train, X_val=X_val, y_train=y_train, y_val=y_val )
        del imgs_train, imgs_mask_train
    else:
        print('Loading predefined training and validation (training) set')
        training_data =  np.load(paths['training_data']+'/training_data.npz')    
        X_train = training_data['X_train']
        X_val = training_data['X_val']
        y_train = training_data['y_train']
        y_val = training_data['y_val'] 
    


    X_train = X_train.astype('float32')
    X_val = X_val.astype('float32')
    y_train = y_train.astype('float32')
    y_val = y_val.astype('float32')
#    
#    np.save(paths['model_save']+'/X_val.npy', X_val)
#    np.save(paths['model_save']+'/y_val.npy', y_val)
    X_train = X_train[...,np.newaxis]
    y_train = y_train[...,np.newaxis]
    X_val = X_val[...,np.newaxis]
    y_val = y_val[...,np.newaxis]
    
    print('-'*30)
    print('Creating and compiling model...')
    print('-'*30)
    model = select_model(training_params,image_params)
  #  model.load_weights(paths['training_data']+'/unet_axial.hdf5')
    #prepare data for the 
 
    data_generator_train = data_generator(X_train, y_train, augmentation_params, training_params['batch_size'])    
    
    model_checkpoint = ModelCheckpoint(filepath=(paths['model_save']+'/unet2D_'+image_params['direction']+'.hdf5'), monitor='val_loss', save_best_only=True)
    loss_history = LossHistory(paths['model_save'])  
    print('-'*30)
    print('Fitting model...')
    print('-'*30)


    model.fit_generator(generator = data_generator_train, 
                        steps_per_epoch = X_train.shape[0]//training_params['batch_size'],
                        epochs = training_params['epoch_number'],
                        verbose=1,
                        callbacks=[model_checkpoint, loss_history],
                        validation_data=(X_val, y_val))
    
def start_batch(input_folder, output_folder): 


    paths = {
            'training_data': input_folder,
            'model_save': output_folder
            }
    
    if not os.path.exists(paths['model_save']):
        os.makedirs(paths['model_save'])

    image_params = {
                    'img_rows': 256, 
                    'img_cols': 256,
                    'img_slices':1104,
                    'direction':'axial',
                    'featurewise_normalization': False,
                    'samplewise_intensity_normalization': True
                    }
    
    augmentation_params = {
                            'noise': True,
                            'rotation': True,
                            'fliplr': False,
                            'min_noise_var':0.001,
                            'max_noise_var':0.01,
                            'noise_prob': 0.3,
                            'min_angle': -10,
                            'max_angle': 10,
                            'rotation_prob': 0.3,
                            'fliplr_prob': 0.5,
                            'zoom_prob': 0.3,
                            'min_zoom': 0.8,
                            'max_zoom': 1.3,
                            'crop_prob': 0.3,
                            'min_crop': 180,
                            'max_crop': 250,
                            'zoom_axis_same': True
                        }
    
    
    training_params = {
                        'val_fraction':0.2,
                        'architecture': 'attn_reg_ds',
                        'dilate_rate': 2,
                        'learning_rate': 1e-5,
                        'batch_size': 20,
                        'epoch_number': 600,
                        'weights_file': 'unet2D',
                        'split': True
                    }
    comments = { 
            '1':'Entrenamiento 2D, axial'}
      
    #check if output path exists, else create it
    if not os.path.exists(paths['model_save']):
     os.makedirs(paths['model_save'])
    print(paths['model_save'])
    train(paths,image_params,augmentation_params,training_params, comments)
    print('training should be finished')