#!/usr/bin/python

import cv2

def start_batch(inputfile, outputprefix):
  print('OpenCV version: ' + str(cv2.__version__))
  vidcap = cv2.VideoCapture(inputfile)
  success,image = vidcap.read()
  count = 0
  success = True
  while success:
    cv2.imwrite("%s%d.jpg" %(outputprefix, count), image)     # save frame as JPEG file
    success,image = vidcap.read()
    count += 1
    
