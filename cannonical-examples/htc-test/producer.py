#!/usr/bin/env python
import os
import sys
import redis_primage_htc_queue
from PRIMAGE_Batch_Args import PRIMAGE_Batch_Args 

primage_htc_batch_queue_service = sys.argv[1]  
queue_name                      = sys.argv[2]  
path_input_folder_images        = sys.argv[3] 
path_output_folder              = sys.argv[4] 

primage_htc_queue = redis_primage_htc_queue.RedisWQ(name=queue_name, host=primage_htc_batch_queue_service)
print("Push with sessionID: " +  primage_htc_queue.sessionID())
print("Initial queue state: empty=" + str(primage_htc_queue.empty()))

for root, dirs, files in os.walk(path_input_folder_images, topdown=False):
   for name in files:
      arg          = PRIMAGE_Batch_Args()              
      arg.arg1     = os.path.join(path_input_folder_images, os.path.join(root, name))
      arg.arg2     = os.path.join(path_output_folder, name)

      print('- Pushing PRIMAGE Batch Argument Object:' + str(arg))
      primage_htc_queue.push(str(arg))
print("Queue completed")

