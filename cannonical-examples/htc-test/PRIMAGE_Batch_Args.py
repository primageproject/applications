class PRIMAGE_Batch_Args:
    arg1 = ''    #Argument 1. Path of the image to process
    arg2 = ''    #Argument 2. Path and prefix for the output files
   
    def __str__(self): # This function convert all arguments to single string
        result =  '[' + str(self.arg1) + ']' + ',' + \
                  '[' + str(self.arg2) + ']'
        return result

    def __init__(self, itemstr = ''):
        l = len(itemstr)
        if l > 0:
           listargs = itemstr[1:len(itemstr)-1].split("],[");
           self.arg1 = listargs[0];
           self.arg2 = listargs[1];

