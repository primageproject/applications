#!/usr/bin/env python
import os
import sys
import redis_primage_htc_queue
from PRIMAGE_Batch_Args import PRIMAGE_Batch_Args #This is same class that you have implemented in the Producer Job
from batch import start_batch

#These are the unique two input arguments for any consumer
primage_htc_batch_queue_service = sys.argv[1] #"PRIMAGE_HTC_BATCH_QUEUE_SERVICE"
queue_name                      = sys.argv[2] #"QUEUE_NAME"

#First, the consumer connect to the Queue named $QUEUE_NAME from PRIMAGE HTC Queue Service (values from $PRIMAGE_HTC_BATCH_QUEUE_SERVICE).
primage_htc_queue = redis_primage_htc_queue.RedisWQ(name=queue_name, host=primage_htc_batch_queue_service)
print("Push with sessionID: " +  primage_htc_queue.sessionID())
print("Initial queue state: empty=" + str(primage_htc_queue.empty()))

"""Here, you have to implement an iterative process where , one by one, the PRIMAGE Batch Arguments Objects are pulled and processed by 
   a Batch Subprocess
"""

while not primage_htc_queue.empty():
    item = primage_htc_queue.lease(lease_secs=10, block=True, timeout=2) 
    if item is not None:
        itemstr = item.decode("utf-8")
        args = PRIMAGE_Batch_Args(itemstr)
        print("Working on PRIMAGE Aguments Object:")
        print("Args: " + args.arg1)
        print("Args: " + args.arg2)
        # execute your User Application.
        start_batch(args.arg1, args.arg2);                 
        primage_htc_queue.complete(item)
    else:
        print("Waiting for work")
        print("Queue empty, exiting")

