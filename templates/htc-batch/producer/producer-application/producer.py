#!/usr/bin/env python

import os
import sys
import redis_primage_htc_queue
from PRIMAGE_Batch_Args import PRIMAGE_Batch_Args #this is the class that you have to employ to generate the PRIMAGE Batch Arguments Objects

#These two input arguments are mandatory for all producers
primage_htc_batch_queue_service = sys.argv[1]   #First argument corresponds to the Queue service ($PRIMAGE_HTC_BATCH_QUEUE_SERVICE)
queue_name                      = sys.argv[2]   #Second argument corresponds to the name of the queue ($QUEUE_NAME)
"""Here, you have to add new input arguments if they are necessary. They will be used to generate PRIMAGE Batch Arguments Objects 
   - Example: The $INPUT argument provides a base path of PRIMAGE Storage where a set of images are saved. The producer
              will generate one PRIMAGE Batch Arguments Object with each image path.
              The $OUTPUT argument provide the base path where the file results produced by the Batch Subprocesses of the consumer will be generated.  
	#Specific input arguments for this producer
  path_input_folder         = sys.argv[3] #It corresponds to $INPUT
  path_output_folder        = sys.argv[4] #It corresponds to $OUTPUT
"""

#First, the producer creates a Queue named $queue_name from $primage_htc_batch_queue_service 
primage_htc_queue = redis_primage_htc_queue.RedisWQ(name=queue_name, host=primage_htc_batch_queue_service)
print("Push with sessionID: " +  primage_htc_queue.sessionID())
print("Initial queue state: empty=" + str(primage_htc_queue.empty()))


"""Here, you have to implement the python code to generate the PRIMAGE Batch Arguments Objects and push them to the Queue.
   - Example: Next code generates one PRIMAGE Batch Arguments Object per image. All images are located in the
              $path_input_folder. The result file will be generated in $path_output_folder.
              The code discard the hidden files.

files = os.listdir(path_input_folder)
for f in files:
    if not f.startswith('.'):#discard hidden files
        arg = PRIMAGE_Batch_Args()
        arg.arg1 = os.path.join(path_input_folder, f)
        arg.arg2 = path_output_folder;
        print('- Pushing PRIMAGE Batch Argument Object:' + str(arg))
        primage_htc_queue.push(str(arg))

print("All PRIMAGE Batch Arguments Objects has been pushed to the Queue")
"""