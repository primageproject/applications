"""
This class is for creating objects to save the required arguments of a single User's Application 
"""
class PRIMAGE_Batch_Args:
    
    """*******************************************************
    Here, you have to define all required arguments for executing a single User's Application.
    In the class, all arguments are defined as Strings.
    - Example: We define four arguments
    arg1 = ''    #Argument 1
    arg2 = ''    #Argument 2
    arg3 = ''    #Argument 3
    arg4 = ''    #Argument 4
    and so on
    ********************************************************"""
    def __str__(self): # This function convert all arguments to single string
        """*****************************************************
        It creates a string from the arguments defined. Each argument must be a string between [] and separated by comma ','
        - Example based on arguments defined above: 
        
        result =  '[' + str(self.arg1) + ']' + ',' \
                + '[' + str(self.arg2) + ']' + ',' \
                + '[' + str(self.arg3) + ']' + ',' \
                + '[' + str(self.arg4) + ']'
        ********************************************************"""
        return result

     def __init__(self, itemstr=""): # This constructor parse an String and assign values to the defined arguments
        l = len(itemstr)
        if (l!=0):
        	listargs = itemstr[1:len(itemstr)-1].split("],[");
        	""""
        	self.arg1 = listargs[0];
        	self.arg2 = listargs[1];
        	self.arg3 = listargs[2];
        	so on
        	"""