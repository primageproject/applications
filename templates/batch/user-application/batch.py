#!/usr/bin/env python
import sys
"""
If applicable, you have to import all libraries and modules where is implemented the User's Application 
"""

"""
#Here, you get the arguments from the user Comman Line Interface
#Example: The arguments are an Input folder and Output folder

input_folder  = sys.argv[1]   #the first argument is the input folder path
output_folder = sys.argv[2]   #the second argument is the output folder path
"""

""" 
Here, you have to implement the User's Application or call the python module (Batch Task) where is implemented.
"""